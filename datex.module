<?php

if (defined('DATEX_DEBUG')) {
  define('DATEX_USE_INTL', defined('DATEX_FORCE_DE_INTL')
    || !class_exists('IntlDateFormatter'));
}
else {
  define('DATEX_USE_INTL', class_exists('IntlDateFormatter'));
}

require_once 'calendar.inc';
require_once 'datex_date.inc';
require_once 'datex_popup.inc';

/**
 * Implements hook_menu().
 *
 * For admin forms.
 */
function datex_menu() {
  $items['admin/config/regional/date-time/datex'] = [
    'title' => 'Datex',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['datex_admin_form'],
    'access arguments' => ['administer site configuration'],
    'type' => MENU_LOCAL_TASK,
    'file' => 'datex.admin.inc',
  ];
  $items['admin/config/regional/date-time/datex/edit/%'] = [
    'title' => 'Datex edit schema',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['datex_schema_edit_form', 6],
    'access arguments' => ['administer site configuration'],
    'type' => MENU_CALLBACK,
    'file' => 'datex.admin.inc',
  ];
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function datex_views_api() {
  return [
    'api' => 3,
    'path' => drupal_get_path('module', 'datex'),
  ];
}

/**
 * Implements hook_views_data_alter().
 */
function datex_views_data_alter(&$data) {
  if (_datex_is_disabled('views') || _datex_is_patching_mode()) {
    return;
  }
  foreach ($data as $key => &$value) {
    if (is_array($value)) {
      datex_views_data_alter($value);
    }

    if ($value === 'date_views_argument_handler_simple') {
      $value = '_datex_date_views_argument_handler_simple';
    }

    // Field Formatter --------------------------------------------------------
    $change = [
      'year',
      'month',
      'day',
      'fulldate',
      'year_month',
      'week',
    ];
    $change = [
      'views_handler_field_date',
      'views_handler_field_last_comment_timestamp',
      'views_handler_field_history_user_timestamp',
      'views_handler_field_ncs_last_updated',
      //      'views_handler_filter_date',
      //      'views_handler_filter_ncs_last_updated',
      //      'views_handler_filter_history_user_timestamp',
    ];
    if (in_array($value, $change, TRUE)) {
      $value = '_datex_' . $value;
    }

    // Contextual filters -----------------------------------------------------
    $change = [
      'year',
      //        'month',
      //        'day',
      'fulldate',
      'year_month',
      //        'week',
    ];
    foreach ([
               'views_handler_argument_node_created_',
               'views_handler_argument_node_changed_',
             ] as $item) {
      foreach ($change as $g) {
        if ($value === $item . $g) {
          $value = '_datex_' . $value;
        }
      }
    }
  }
}


// ___________________________________________________________________ INTERNAL

/**
 * Calendars supported by PHP-Intl.
 */
function _datex_available_calendars() {
  return [
    'japanese' => t('Japanese'),
    'buddhist' => t('Buddhist'),
    'chinese' => t('Chinese'),
    'persian' => t('Persian'),
    'indian' => t('Indian'),
    'islamic' => t('Islamic'),
    'hebrew' => t('Hebrew'),
    'coptic' => t('Coptic'),
    'ethiopic' => t('Ethiopic'),
  ];
}

/**
 * Get calendar for a/current language in a mode (if given) from schema.
 *
 * @param string $forSchema
 * @param null $forLang
 *
 * @return null
 */
function _datex_language_calendar($forSchema = 'default', $forLang = NULL) {
  if (!$forLang) {
    $forLang = $GLOBALS['language']->language;
  }
  $cfg = variable_get('datex_schema', []);
  return !isset($cfg[$forSchema][$forLang])
    ? NULL
    : $cfg[$forSchema][$forLang];
}

function _datex_is_patching_mode() {
  return variable_get('datex_mode', 0);
}

function _datex_is_disabled($name) {
  $dis = variable_get('datex_disabled');
  if (!isset($dis['datex'])) {
    $dis['datex'] = FALSE;
  }
  if (!isset($dis[$name])) {
    $dis[$name] = FALSE;
  }
  return $dis['datex'] || $dis[$name];
}

// TODO move to admin file.
function _datex_schema_form_options() {
  return [
      'disabled' => t('Disabled'),
      'default' => t('Default'),
    ] + drupal_map_assoc(array_keys(variable_get('datex_schema', [])));
}

// _______________________________________________________________________ MAIN

/**
 * Implements hook_datex_format_alter().
 *
 * This hook is not available unless Drupal core is patched with provided
 * patch in datex module's directory. If it's not desirable to patch the core
 * (which is usually the case) non-patching mode can be used instead.
 *
 * @param $data
 * @param $context
 */
function datex_datex_format_alter(&$data, $context) {
  if (_datex_is_disabled('datex_hook') || !_datex_is_patching_mode()) {
    return;
  }

  $calendar = datex_factory(
    $context['timezone'],
    _datex_language_calendar('default', $context['langcode'])
  );
  if ($calendar) {
    $calendar->setTimestamp($context['timestamp']);
    $data = $calendar->format($context['format']);
  }
}

/**
 * Implements hook_preprocess_node().
 *
 * Localizes 'published on' date in non-patching mode.
 *
 * @param $variables
 */
function datex_preprocess_node(&$variables) {
  if (_datex_is_disabled('node') || _datex_is_patching_mode()) {
    return;
  }

  $calendar = datex_factory();
  if (!$calendar) {
    return;
  }
  $calendar->setTimestamp($variables['created']);

  $format = variable_get('date_format_medium', 'D, m/d/Y - H:i');
  $variables['date'] = $calendar->format($format);
  if (isset($variables['display_submitted'])) {
    $variables['submitted'] = t('Submitted by !username on !datetime', [
      '!username' => $variables['name'],
      '!datetime' => $variables['date'],
    ]);
  }
}

/**
 * Implements hook_preprocess_comment().
 *
 * Localizes 'published on' date in non-patching mode.
 *
 * @param $variables
 */
function datex_preprocess_comment(&$variables) {
  if (_datex_is_disabled('comment') || _datex_is_patching_mode()) {
    return;
  }
  $calendar = datex_factory();
  if (!$calendar) {
    return;
  }

  $calendar->setTimestamp($variables['elements']['#comment']->created);

  // Timestamp set while calling factory.
  $fmt = variable_get('date_format_medium', '');
  $variables['changed'] = $calendar->format($fmt);
  $calendar->setTimestamp($variables['elements']['#comment']->created);
  $variables['created'] = $calendar->format($fmt);
  $variables['submitted'] = t('Submitted by !username on !datetime', [
    '!username' => $variables['author'],
    '!datetime' => $variables['created'],
  ]);
}

function _datex_comment_edit_form_date_validate($f, &$fs) {
  $calendar = datex_factory();
  if (!$calendar) {
    return;
  }

  if (!isset($fs['values']['date'])) {
    return;
  }

  $date = &$fs['values']['date'];

  $m = [];
  $pattern = '#^([0-9]{2,4})[-\/\\\]([0-9]{1,2})[-\/\\\]([0-9]{1,2})( {1,}([0-9]{1,2})\:([0-9]{1,2}))?#';
  $ok = preg_match($pattern, $date, $m);
  if ($ok && count($m) == 7) {
    $calendar->setDateLocale($m[1], $m[2], $m[3]);
    $calendar->setTime($m[5], $m[6], 0);
    $date = $calendar->xFormat('Y-m-d H:i:s O');
  }
  else {
    form_set_error('date', t('Invalid date'));
  }
}

function _datex_node_edit_form_date_validate($el, &$fs, $f) {
  if (!$fs['#datexified'] || empty($el['#value'])) {
    return;
  }

  $calendar = datex_factory();
  if (!$calendar) {
    form_set_error($el['#name'], t('Invalid site language.'));
  }

  $m = [];
  $pattern = '#^([0-9]{2,4})[-\/\\\]([0-9]{1,2})[-\/\\\]([0-9]{1,2})( {1,}([0-9]{1,2})\:([0-9]{1,2})\:([0-9]{1,2}))?#';
  $ok = preg_match($pattern, $el['#value'], $m);
  if ($ok && count($m) == 8) {
    $calendar->setDateLocale($m[1], $m[2], $m[3]);
    $calendar->setTime($m[5], $m[6], $m[7]);
    $el['#value'] = $calendar->xFormat('Y-m-d H:i:s O');
    form_set_value($el, $el['#value'], $fs);
  }
  else {
    form_set_error($el['#name'], t('You have to specify a valid date.'));
  }
}

/**
 * Implements hook_form_alter().
 *
 * Modules to support:
 *  - Scheduler
 *  - translation date
 *  - views exposed forms
 *
 * @param $f
 * @param $fs
 * @param $form_id
 */
function datex_form_alter(&$f, &$fs, $form_id) {
  $calendar = datex_factory();
  if (!$calendar) {
    return;
  }

  if ($form_id === 'node_admin_content' && isset($f['admin']['nodes']['#options'])) {
    // Contents list (/admin/content/).
    if (_datex_is_patching_mode() || _datex_is_disabled('node_admin')) {
      return;
    }
    $format = variable_get('date_format_short', '');
    if (!$format) {
      return;
    }
    foreach ($f['admin']['nodes']['#options'] as &$node) {
      $date = &$node['changed'];
      $date_obj = date_create_from_format($format, $date);
      $calendar->setTimestamp($date_obj->format('U'));
      $date = $calendar->format($format);
    }
  }
  elseif (isset($f['#node_edit_form'])) {
    $fs['#datexified'] = TRUE;
    if (_datex_is_disabled('node_edit')) {
      return;
    }
    $now = $calendar->format('o-m-d H:i:s O');
    if (isset($f['author']['date'])) {
      $t_args = ['%date' => $now];
      $f['author']['date']['#description'] = t(
        'Format: %date The date format is YYYY-MM-DD and time is H:i:s. Leave blank to use the time of form submission.', $t_args);
      $f['author']['date']['#element_validate'][] = '_datex_node_edit_form_date_validate';
    }
    if (isset($f['scheduler_settings']) && !module_exists('date_popup')) {
      foreach (['publish_on', 'unpublish_on'] as $name) {
        if (isset($f['scheduler_settings'][$name])) {
          $f['scheduler_settings'][$name]['#element_validate'][] = '_datex_node_edit_form_date_validate';
        }
        // If in patching mode, date is already formatted and localized.
        if (!_datex_is_patching_mode()) {
          if (isset($f['#node']->scheduler[$name]) && !empty($f['scheduler_settings'][$name]['#default_value'])) {
            $calendar->setTimestamp($f['#node']->scheduler[$name]);
            $f['scheduler_settings'][$name]['#default_value']
              = $calendar->format('Y-m-d H:i:s O');
          }
          $f['scheduler_settings'][$name]['#description'] = t('Format: %date The date format is YYYY-MM-DD and time is H:i:s. Leave blank to disable scheduled.', ['%date' => $now]);
        }
      }
    }
    if (!_datex_is_patching_mode() && !empty($f['author']['date']['#default_value'])) {
      $calendar->setTimestamp($f['created']['#value']);
      $f['author']['date']['#default_value'] = $calendar->format('Y-m-d H:i:s O');
    }
  }
  elseif (isset($f['#id']) && $f['#id'] === 'comment-form' && $f['#entity'] && $f['#entity']->cid) {
    if (_datex_is_disabled('comment_edit')) {
      return;
    }
    if (!isset($f['#validate'])) {
      $f['#validate'] = [];
    }
    if (isset($f['author']['date'])) {
      $fmt = 'Y-m-d H:i O';
      $date_obj = date_create_from_format($fmt, $f['author']['date']['#default_value']);
      $calendar->setTimestamp($date_obj->format('U'));
      $f['author']['date']['#default_value'] = $calendar->format($fmt);
    }
    array_unshift($f['#validate'], '_datex_comment_edit_form_date_validate');
  }
}

// ______________________________________________________________________ TOKEN
